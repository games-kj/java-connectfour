# java-connectfour


This is a java implementation of the classic game of connect-four.
The usecase-agnostic approach revolves about the general rules, some infrastructure and agents.

The fastest way to reach a playable game is using the `main` function of the `Game` class.
The `api` package is mainly designed for CLI usage and provides info about games and interactions with agents.
As there is currently virtually no documentation and only very basic error handling for the API, it is mainly useful for automated calls, such as done by [this website](https://en.johannes-kuenel.de/projects/chess/).

While there are commonly used board dimensions, this implementation allows for setting them.
In the `main` function of the `Game` class, they can be changed by altering the code.
The API also allows for setting them.
The above-mentioned website allows for easy adjustment.

The code is at the state of the first working solution, i.e. many optimizations can be done, such as adjusting the variable scopes.