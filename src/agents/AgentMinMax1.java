/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;
import logics.Board;
import logics.Move;

/**
 *
 * @author Johannes
 */

// Agent implementing the MinMax algorithm with some tweaks
public class AgentMinMax1 extends Agent {
    
    public int maxDepth = 2;
    public int maxDepthInitial = 2;
    public long allowedMillis = 10000;
    public Instant timeStart;
    public HashMap<String, Integer> transpositionTable;
    public boolean timeUp;
    public Move chosenMove;
    
    public final int BOUND_WIN = 9999999;
    static final int SIGNAL_WIN = 99999999;
    public final int SIGNAL_IMPOSSIBLE = 999999999;
    
    @Override
    public Move chooseMove() {

        this.transpositionTable = new HashMap<>();
        
        if (this.board.numPieces() == 0) {
            return new Move(board.width / 2);
        }
        else System.out.println("Found " + board.numPieces() + " pieces");
        
        this.transpositionTable = new HashMap<>();
        this.maxDepth = this.maxDepthInitial;
        this.timeUp = false;

        this.chosenMove = this.board.getPossibleMoves().get(0);
        this.chosenMove.estimationLongterm = (this.playerNr == 0 ? -SIGNAL_IMPOSSIBLE : SIGNAL_IMPOSSIBLE);
        
        this.timeStart = Instant.now();
        
        int estimation;
        
        int nrCalculationRounds = 1;
        
        System.out.println("----- Starting Calculation -----");
        
        if (this.board.getPossibleMoves().size() == 1) {
            return this.board.getPossibleMoves().get(0);
        }

        // While time is not up and no win or loss has been found: Perform MinMax with increasing depths (starting over in each iteration)
        while (true) {
            
            System.gc();
            
            System.out.println("Starting MinMax Calculation round " + nrCalculationRounds + " with maxDepth=" + this.maxDepth + ", considering " + this.board.getPossibleMoves().size() + " moves");

            // Invoke MinMax, starting according to player color
            if (this.playerNr == 0) {
                estimation = this.maximize(this.board, 0, BOUND_WIN);
            }
            else {
                estimation = this.minimize(this.board, 0, -BOUND_WIN);
            }

            // No more time for further iterations
            if (timeUp) {
                System.out.println("Time is up, expecting " + this.chosenMove.estimationLongterm);
                break;
            }

            // Found a winning strategy => Don't need to search further
            if ((this.playerNr == 0 && estimation >= BOUND_WIN) || (this.playerNr == 1 && estimation <= -BOUND_WIN)) {
                System.out.println("Quitting after this round since win is already sure");
                break;
            }
            // Found out that there is a winning strategy for the enemy => Just go with best found move
            if ((this.playerNr == 1 && estimation >= BOUND_WIN) || (this.playerNr == 0 && estimation <= -BOUND_WIN)) {
                System.out.println("Oh no, I will loose, but I'm trying to survive as long as possible");
                break;
            }

            // Sort moves according to estimation of most recent MinMax iteration, which might help with pruning
            if (this.playerNr == 0) {
                this.board.possibleMoves = (ArrayList<Move>)this.board.possibleMoves.stream().filter(mv -> mv.estimationLongterm > -BOUND_WIN).collect(Collectors.toList());
                this.board.possibleMoves.sort((a, b) -> b.estimationLongterm - a.estimationLongterm);
            }
            else {
                this.board.possibleMoves = (ArrayList<Move>)this.board.possibleMoves.stream().filter(mv -> mv.estimationLongterm < BOUND_WIN).collect(Collectors.toList());
                this.board.possibleMoves.sort((a, b) -> a.estimationLongterm - b.estimationLongterm);
            }
            
            if (this.board.possibleMoves.size() <= 1) {
                System.out.println("Only " + this.board.possibleMoves.size() + " move(s) not leading to loosing, so not much to think about");
                break;
            }

            // Clear transposition table for next iteration
            this.transpositionTable.clear();

            // Dig deeper on next iteration
            this.maxDepth++;
            nrCalculationRounds ++;
        }
        
        return this.chosenMove;
    }

    // TODO: Maybe combine maximize and minimize functions to avoid duplicate code

    // Max-part of MinMax
    public int maximize(Board board, int depth, int foundMin) {
        
        int res = -SIGNAL_IMPOSSIBLE;
        Move bestMove = null;
        
        String transRep;

        // Go through available next moves
        for (Move mv : board.getPossibleMoves()) {
            
            transRep = transpositionRepresentation(mv.resultingBoard);

            // If move is present in transposition table: Use already calculated value
            if (this.transpositionTable.get(transRep) != null) {
                mv.estimationLongterm = this.transpositionTable.get(transRep);
            }
            else {
                // If maximum depth is reached or game is decided: Use heuristic function
                if (depth >= this.maxDepth || mv.resultingBoard.isDecided()) {
                    mv.estimationLongterm = mv.resultingBoard.getEstimation();
                }
                else {
                    // Go to next depth with counterpart function
                    mv.estimationLongterm = this.minimize(mv.resultingBoard, depth + 1, res);
                }

                // Put move into transposition table
                this.transpositionTable.put(transRep, mv.estimationLongterm);
            }

            // If move looks better than previous favorite: Make it new favorite
            if (mv.estimationLongterm > res) {
                res = mv.estimationLongterm;
                bestMove = mv;
                // If we are at depth 0, i.e. moves are actual possible moves: Do it
                if (depth == 0 && res > this.chosenMove.estimationLongterm) {
                    this.chosenMove = bestMove;
                }
                // Aplha-Beta-pruning
                if (res >= foundMin) break;
            }

            // If we are at depth 0 and the time is up: Wrap it up
            if (depth == 0 && Duration.between(this.timeStart, Instant.now()).toMillis() > this.allowedMillis) {
                this.timeUp = true;
                break;
            }
        }

        // Set chosen move just in case it was not set earlier
        if (res == SIGNAL_WIN) {
            res -= depth;
        }
        else if (res == -SIGNAL_WIN) {
            res += depth;
        }

        // Memory saving and trying to avoid unexpected side effects of move caching
        if (depth > 0) {
            board.possibleMoves.clear();
            board.gotPossibleMoves = false;
        }
        
        return res;
    }

    // Min-part of MinMax
    public int minimize(Board board, int depth, int foundMax) {
        
        int res = SIGNAL_IMPOSSIBLE;
        Move bestMove = null;
        
        String transRep;
        
        for (Move mv : board.getPossibleMoves()) {
            
            transRep = transpositionRepresentation(mv.resultingBoard);

            if (this.transpositionTable.get(transRep) != null) {
                mv.estimationLongterm = this.transpositionTable.get(transRep);
            }
            else {
                if (depth >= this.maxDepth || mv.resultingBoard.isDecided()) {
                    mv.estimationLongterm = mv.resultingBoard.getEstimation();
                }
                else {
                    mv.estimationLongterm = this.maximize(mv.resultingBoard, depth + 1, res);
                }
                
                this.transpositionTable.put(transRep, mv.estimationLongterm);
            }
            
            if (mv.estimationLongterm < res) {
                res = mv.estimationLongterm;
                bestMove = mv;
                if (depth == 0 && res < this.chosenMove.estimationLongterm) {
                    this.chosenMove = bestMove;
                }
                if (res <= foundMax) break;
            }
            
            if (depth == 0 && Duration.between(this.timeStart, Instant.now()).toMillis() > this.allowedMillis) {
                this.timeUp = true;
                break;
            }
        }
        
        if (res == SIGNAL_WIN) {
            res -= depth;
        }
        else if (res == -SIGNAL_WIN) {
            res += depth;
        }
        
        if (depth > 0) {
            board.possibleMoves.clear();
            board.gotPossibleMoves = false;
        }
        
        return res;
    }
    
    public static String transpositionRepresentation(Board b) {

        return Arrays.deepToString(b.fields);
    }
}
