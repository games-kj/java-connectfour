/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import logics.Board;
import logics.Move;

/**
 *
 * @author Johannes
 */

// Simplistic Agent just for testing
public class AgentOneMove extends Agent {
    
    @Override
    public Move chooseMove() {
        
        try {
            TimeUnit.MILLISECONDS.sleep(100);
            
        } catch (InterruptedException ex) {
            Logger.getLogger(AgentOneMove.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Board workboard = new Board(this.board);
        
        List<Move> possibleMoves = workboard.getPossibleMoves();
        
        if (this.playerNr == 1) {
            possibleMoves.sort(Comparator.comparing(Move::getGain));
        }
        else {
            possibleMoves.sort(Comparator.comparing(Move::getGain).reversed());
        }
        
        List<Move> bestMoves = new ArrayList<Move>();
        int estimatedGain = possibleMoves.get(0).getGain();
        for (Move mv : possibleMoves) {
            if (mv.getGain() == estimatedGain) {
                bestMoves.add(mv);
            }
            else {
                break;
            }
        }

        Random rand = new Random();
        int pickedNr = rand.nextInt(bestMoves.size());
        
        return bestMoves.get(pickedNr);
    }
}
