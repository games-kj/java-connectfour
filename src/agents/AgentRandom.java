/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import logics.Move;

/**
 *
 * @author Johannes
 */

// Simplistic Agent just for testing
public class AgentRandom extends Agent {
    
    @Override
    public Move chooseMove() {
        
        try {
            TimeUnit.MILLISECONDS.sleep(100);
            
        } catch (InterruptedException ex) {
            Logger.getLogger(AgentRandom.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        List<Move> possibleMoves = this.board.getPossibleMoves();
        
        Random rand = new Random();
        int pickedNr = rand.nextInt(possibleMoves.size());
        
        Move pickedMove = possibleMoves.get(pickedNr);
        
        return pickedMove;
    }
}
