/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import logics.Field;
import logics.Move;

/**
 *
 * @author Johannes
 */

// Agent that reads the next move from a CLI user
public class Human extends Agent {
    
    @Override
    public Move chooseMove() {
        
        System.out.println("Player " + this.playerNr + ", please enter your move:");

        // Keep reading moves until a valid one is put in
        while (true) {
            
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String readMove;
            
            try {
                readMove = reader.readLine();
            } catch (IOException ex) {
                readMove = "";
                Logger.getLogger(Human.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            int intMove = -1;
            
            try {
                intMove = Integer.parseInt(readMove);
            } catch (NumberFormatException ex) {
                System.out.println("Invalid format, expecting number of the column (left column = 1). Please try again:");
                continue;
            }
            
            if (intMove < 1 || intMove > this.board.width) {
                System.out.println("Move has to be between 1 and " + this.board.width + ". Please try again:");
            }
            else {
                return new Move(intMove - 1);
            }
        }
    }
}
