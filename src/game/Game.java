/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import agents.Agent;
import agents.AgentMinMax1;
import agents.Human;
import gui.GBoard;
import logics.Board;
import logics.Move;

/**
 *
 * @author Johannes
 */

// Represents a game of connect four, letting two agents compete and managing a GUI
public class Game {
    
    public GBoard gui;
    public Board board;
    
    public Agent player1;
    public Agent player2;
    public int turn = 0;
    public int winner = -1;
    
    public Game(Agent player1, Agent player2) {
        this.player1 = player1;
        this.player2 = player2;
    }
    
    public void start(){
        
        if (this.gui != null) {
            this.gui.close();
        }
        
        this.board = new Board();
        
        this.player1.init(board, 0);
        this.player2.init(board, 1);
        
        this.gui = new GBoard(this.board);
        this.gui.update();
        
        while(true) {
            this.doTurn();
            this.gui.update();
            this.board.getPossibleMoves();
            this.board.getEstimation();
            System.out.println("--- Board estimation: " + this.board.getEstimation() + " ---");
            if (this.board.getResult() >= 0) {
                break;
            }
        }
        if (this.board.getResult() == 2) {
            System.out.println("IT'S A TIE");
        }
        else {
            System.out.println("WINNER: player" + this.board.getResult());
        }
    }
    
    public void doTurn() {
        
        Move move;
        
        while(true) {
            move = (turn == 0 ? player1.chooseMove() : player2.chooseMove());
            if (this.board.move(move)) {
                break;
            }
            System.out.println("Board rejected the move (" + move.column + ")");
        }
        
        this.turn ^= 1;
    }

    // If you use this as the project's main function, you can set which players will compete here
    public static void main(String[] args) {
        Agent player1 = new Human();
        Agent player2 = new AgentMinMax1();
        Game game = new Game(player1, player2);
        game.start();
    }
}
