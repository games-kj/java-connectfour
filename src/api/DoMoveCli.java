/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import static functions.Functions.parse2dJsonIntArray;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import logics.Board;
import logics.Move;

/**
 *
 * @author Johannes
 */

// Accepts a move, a board and the number of the player (1 for first, 2 for second) as arguments, chooses a move, applies the move to the board and prints the results
// TODO: Documentation of input and output format and parsing details
public class DoMoveCli {
    
    public static void main(String[] args) {
        
        int move;
        int[][] fields;
        int player_nr;
        int allowed_ms;
        
        System.out.println("Starting with args " + Arrays.toString(args));
        try {
            move = Integer.parseInt(args[0]);
            fields = parse2dJsonIntArray(args[1]);
            player_nr = Integer.parseInt(args[2]) - 1;
            
            int foundHeight = -1;
            for (int[] col : fields) {
                if (foundHeight < 0) {
                    foundHeight = col.length;
                }
                else if (col.length != foundHeight) {
                    throw new Exception();
                }
                for (int field_nr = 0; field_nr < col.length; field_nr++) {
                    col[field_nr]--;
                }
            }
            
            if (move < 0 || move >= fields.length) {
                throw new Exception();
            }
            
            if (player_nr != 0 && player_nr != 1) {
                throw new Exception();
            }
        }
        catch(Exception e) {
            System.out.println("-1");
            return;
        }
        
        int[][] returnedFields;
        ArrayList<Move> returnedMovesPossible;
        int returnedResult;
        int returnedTurn;
        
        try {
            Board board = new Board(fields, player_nr);
            
            if (!board.move(new Move(move))) {
                System.out.println("-4");
                return;
            }
            
            returnedFields = new int[board.width][board.height];
            for (int col = 0; col < board.fields.length; col++) {
                for (int row = 0; row < board.fields[0].length; row++) {
                    returnedFields[col][row] = board.fields[col][row] + 1;
                }
            }
            
            returnedMovesPossible = board.getPossibleMoves();
            
            returnedResult = board.getResult() + 1;
            
            returnedTurn = board.turn + 1;
        }
        catch(Exception e) {
            System.out.println("-2");
            return;
        }
        
        try {
            String textFields = Arrays.deepToString(returnedFields).replace(" ", "");
            String textMovesPossible =  returnedMovesPossible.stream().map(mv -> mv.column).collect(Collectors.toList()).toString().replace(" ", "");
            String textResult = Integer.toString(returnedResult);
            String textTurn = Integer.toString(returnedTurn);
            
            System.out.println("" + textFields + " " + textMovesPossible + " " + textResult + " " + textTurn);
        }
        catch(Exception e) {
            System.out.println("-3");
        }
    }
}
