/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import static functions.Functions.parse2dJsonIntArray;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import logics.Board;
import logics.Move;

/**
 *
 * @author Johannes
 */

// Accepts a board as arguments, chooses a move, prints the possible moves and status of the board
// TODO: Documentation of input and output format and parsing details
public class StatusCli {
    
    public static void main(String[] args) {
        
        int[][] fields;
        
        System.out.println("Starting with args " + Arrays.toString(args));
        try {
            fields = parse2dJsonIntArray(args[0]);
            
            int foundHeight = -1;
            for (int[] col : fields) {
                if (foundHeight < 0) {
                    foundHeight = col.length;
                }
                else if (col.length != foundHeight) {
                    throw new Exception();
                }
                for (int field_nr = 0; field_nr < col.length; field_nr++) {
                    col[field_nr]--;
                }
            }
        }
        catch(Exception e) {
            System.out.println("-1");
            return;
        }
        
        int returnedResult;
        ArrayList<Move> returnedMovesPossible;
        
        try {
            Board board = new Board(fields, 0);
            
            returnedMovesPossible = board.getPossibleMoves();
            
            returnedResult = board.getResult() + 1;
        }
        catch(Exception e) {
            System.out.println("-2");
            return;
        }
        
        try {
            String textMovesPossible =  returnedMovesPossible.stream().map(move -> move.column).collect(Collectors.toList()).toString().replace(" ", "");
            String textResult = Integer.toString(returnedResult);
            
            System.out.println(textMovesPossible + " " + textResult);
        }
        catch(Exception e) {
            System.out.println("-3");
        }
    }
}
