/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import agents.AgentMinMax1;
import static functions.Functions.parse2dJsonIntArray;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import logics.Board;
import logics.Move;

/**
 *
 * @author Johannes
 */

// Accepts a board, the number of the player (1 for first, 2 for second) and the number of allowed ms to think as arguments, chooses a move, applies the move to the board and prints the results
// TODO: Documentation of input and output format and parsing details
public class AgentCli {
    
    public static void main(String[] args) {
        
        int[][] fields;
        int player_nr;
        int allowed_ms;
        
        System.out.println("Starting with args " + Arrays.toString(args));
        try {
            fields = parse2dJsonIntArray(args[0]);
            player_nr = Integer.parseInt(args[1]) - 1;
            allowed_ms = Integer.parseInt(args[2]);
            
            int foundHeight = -1;
            for (int[] col : fields) {
                if (foundHeight < 0) {
                    foundHeight = col.length;
                }
                else if (col.length != foundHeight) {
                    throw new Exception();
                }
                for (int field_nr = 0; field_nr < col.length; field_nr++) {
                    col[field_nr]--;
                }
            }
            
            if (player_nr != 0 && player_nr != 1) {
                System.out.println("Player number should be 0 or 1, but was " + player_nr);
                throw new Exception();
            }
            
            if (allowed_ms < 100 || allowed_ms > 60000) {
                throw new Exception();
            }
        }
        catch(Exception e) {
            System.out.println("-1");
            return;
        }
        
        Move returnedMove;
        int[][] returnedFields;
        ArrayList<Move> returnedMovesPossible;
        int returnedResult;
        int returnedTurn;
        
        try {
            Board board = new Board(fields, player_nr);
            
            if (board.getResult() >= 0) {
                String textMove = "-1";
                returnedFields = new int[board.width][board.height];
                for (int col = 0; col < board.fields.length; col++) {
                    for (int row = 0; row < board.fields[0].length; row++) {
                        returnedFields[col][row] = board.fields[col][row] + 1;
                    }
                }
                String textFields = Arrays.deepToString(returnedFields).replace(" ", "");
                String textMovesPossible =  "[]";
                String textResult = Integer.toString(board.getResult() + 1);
                String textTurn = "0";
            
                System.out.println(textMove + " " + textFields + " " + textMovesPossible + " " + textResult + " " + textTurn);
                return;
            }
            
            AgentMinMax1 agent = new AgentMinMax1();
            agent.init(board, player_nr);
            agent.allowedMillis = allowed_ms;
            
            returnedMove = agent.chooseMove();
            
            board.move(returnedMove);
            
            returnedFields = new int[board.width][board.height];
            for (int col = 0; col < board.fields.length; col++) {
                for (int row = 0; row < board.fields[0].length; row++) {
                    returnedFields[col][row] = board.fields[col][row] + 1;
                }
            }
            
            returnedMovesPossible = board.getPossibleMoves();
            
            returnedResult = board.getResult() + 1;
            
            returnedTurn = board.turn + 1;
        }
        catch(Exception e) {
            System.out.println("-2");
            return;
        }
        
        try {
            String textMove = Integer.toString(returnedMove.column);
            String textFields = Arrays.deepToString(returnedFields).replace(" ", "");
            String textMovesPossible =  returnedMovesPossible.stream().map(move -> move.column).collect(Collectors.toList()).toString().replace(" ", "");
            String textResult = Integer.toString(returnedResult);
            String textTurn = Integer.toString(returnedTurn);
            
            System.out.println(textMove + " " + textFields + " " + textMovesPossible + " " + textResult + " " + textTurn);
        }
        catch(Exception e) {
            System.out.println("-3");
        }
    }
}
