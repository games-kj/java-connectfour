/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functions;

/**
 *
 * @author Johannes
 */
public class Functions {

    // Converts a 2D json array into a 2D int array
    public static int[][] parse2dJsonIntArray(String json) {
        
        int[][] res;
        
        json = json.replace("\"", "");
        json = json.replace("'", "");
        json = json.replace(" ", "");
        
        String[] jsonCols = json.split("],\\[");
        res = new int[jsonCols.length][];
        
        String[] jsonFields;
        int nrCol = 0;
        int nrField;
        for (String jsonCol : jsonCols) {
            jsonCol = jsonCol.replace("[", "");
            jsonCol = jsonCol.replace("]", "");
            jsonFields = jsonCol.split(",");
            res[nrCol] = new int[jsonFields.length];
            nrField = 0;
            for (String jsonField : jsonFields) {
                res[nrCol][nrField] = Integer.parseInt(jsonField);
                nrField++;
            }
            nrCol++;
        }
        
        return res;
    }
}
