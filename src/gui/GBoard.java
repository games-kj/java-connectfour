/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Dimension;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import logics.Board;

/**
 *
 * @author Johannes
 */

// Graphical board representation
public class GBoard extends JFrame {
    
    public JFrame board;
    public int fieldsize = 100;
    public int coinsize = 90;
    public Board logicBoard;
    public JPanel panel;
    
    public GBoard(Board logicBoard) {
        
        this.logicBoard = logicBoard;
        
        //this.setSize(fieldsize * this.logicBoard.width, fieldsize * this.logicBoard.height);
        this.panel = new GPanel(this.logicBoard, this.fieldsize, this.coinsize);
        this.panel.setPreferredSize(new Dimension(fieldsize * this.logicBoard.width, fieldsize * this.logicBoard.height));
        this.add(this.panel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }
    
    public void update() {
        
        this.repaint();
    }
    
    public void close() {
        
        this.board.dispatchEvent(new WindowEvent(this.board, WindowEvent.WINDOW_CLOSING));
    }
}
