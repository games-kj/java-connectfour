/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
import logics.Board;

/**
 *
 * @author Johannes
 */

// Graphical panel, mostly as a helper for the graphical board
public class GPanel extends JPanel {
    
    public int fieldsize;
    public int coinsize;
    public Board logicBoard;
    
    public GPanel(Board logicBoard, int fieldsize, int coinsize) {
        
        this.logicBoard = logicBoard;
        this.fieldsize = fieldsize;
        this.coinsize = coinsize;
    }
    
    
    @Override
    public void paint(Graphics g) {
        
        g.setColor(Color.DARK_GRAY);
        g.fillRect(0, 0, this.fieldsize * this.logicBoard.width, this.fieldsize * this.logicBoard.height);
        
        Color fieldColor;
        
        for (int y = 0; y < this.logicBoard.height; y++) {
            for (int x = 0; x < this.logicBoard.width; x++) {
                switch (this.logicBoard.fields[x][y]) {
                    case 0:
                        fieldColor = Color.RED;
                        break;
                    case 1:
                        fieldColor = Color.YELLOW;
                        break;
                    default:
                        fieldColor = Color.WHITE;
                }
                g.setColor(fieldColor);
                g.fillOval(x * this.fieldsize + this.fieldsize / 2 - coinsize / 2, (this.logicBoard.height - y - 1) * this.fieldsize + this.fieldsize / 2 - coinsize / 2, this.coinsize, this.coinsize);
            }
        }
    }
}
