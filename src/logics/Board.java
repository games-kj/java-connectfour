/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logics;

import java.util.ArrayList;

/**
 *
 * @author Johannes
 */

// Logical  representation of a checkers board with its pieces, moves, etc.
public class Board {
    
    public int[][] fields;
    public int width;
    public int height;
    public int turn;
    public int result;
    public int estimation;
    public boolean gotEstimation;
    public boolean checkedEnd;
    public ArrayList<Move> possibleMoves;
    public boolean gotPossibleMoves;
    public ArrayList<Group> groups;
    public ArrayList<Field>[] completions;
    
    public static final int SIGNAL_WIN = 99999999;
    public static final int SIGNAL_SURE = 999999;
    public static final int WORTH_COMPLETION = 100;
    public static final int COMPLETION_BONUS_ALIGNED = 60;

    public static final int PLAYER_WHITE = 0;
    public static final int PLAYER_BLACK = 1;

    public static final int PIECE_WHITE = 0;
    public static final int PIECE_BLACK = 1;
    public static final int PIECE_NONE = -1;
    
    
    public Board(int width, int height) {
        
        this.width = width;
        this.height = height;
        
        fields = new int[this.width][this.height];
        
        for (int y = 0; y < this.height; y++) {
            for (int x = 0; x < this.width; x++) {
                this.fields[x][y] = -1;
            }
        }
        
        this.turn = 0;
        
        this.possibleMoves = new ArrayList<Move>();
        this.gotPossibleMoves = false;
        this.gotEstimation = false;
        this.result = -1;
        this.checkedEnd = false;
        this.groups = new ArrayList<Group>();
        this.completions = new ArrayList[2];
        this.completions[0] = new ArrayList<Field>();
        this.completions[1] = new ArrayList<Field>();
    }
    
    public Board() {
        
        this(7, 6);
    }
    
    public Board(int[][] fields, int turn) {
        
        this.fields = fields;
        this.turn = turn;
        
        this.possibleMoves = new ArrayList<Move>();
        this.gotPossibleMoves = false;
        this.gotEstimation = false;
        this.result = -1;
        this.checkedEnd = false;
        this.groups = new ArrayList<Group>();
        this.completions = new ArrayList[2];
        this.completions[0] = new ArrayList<Field>();
        this.completions[1] = new ArrayList<Field>();
        this.width = fields.length;
        this.height = fields[0].length;
    }
    
    public Board(Board boardToClone) {
        
        this.width = boardToClone.width;
        this.height = boardToClone.height;
        
        fields = new int[this.width][this.height];
        
        for (int y = 0; y < this.height; y++) {
            for (int x = 0; x < this.width; x++) {
                this.fields[x][y] = boardToClone.fields[x][y];
            }
        }
        this.turn = boardToClone.turn;
        
        this.possibleMoves = new ArrayList<Move>();
        this.gotPossibleMoves = false;
        this.gotEstimation = false;
        this.checkedEnd = false;
        this.result = boardToClone.result;
        this.groups = new ArrayList<Group>();
        this.completions = new ArrayList[2];
        this.completions[0] = new ArrayList<Field>();
        this.completions[1] = new ArrayList<Field>();
    }

    // Apply move if it is valid
    public boolean move(Move move) {
        
        if (move.column < 0 || move.column >= this.width) return false;

        // Check if column is already full
        for (int y = 0; y < this.height; y++) {
            if (this.fields[move.column][y] < 0) {
                this.fields[move.column][y] = this.turn;
                this.gotPossibleMoves = false;
                this.gotEstimation = false;
                this.checkedEnd = false;
                this.turn ^= 1;
                return true;
            }
        }
        
        return false;
    }

    // Cached moves calculation function
    public ArrayList<Move> getPossibleMoves() {
        
        if (!this.gotPossibleMoves) {
            this.calculateMoves();
        }
        
        return this.possibleMoves;
    }

    //  Moves calculation: Get possible moves of the current player
    public void calculateMoves() {
        
        this.possibleMoves.clear();
        
        for (int x = 0; x < this.width; x++) {
            if (this.fields[x][this.height - 1] < 0) {
                Move calculatedMove = new Move(x);
                calculatedMove.resultingBoard = new Board(this);
                calculatedMove.resultingBoard.move(calculatedMove);
                this.possibleMoves.add(calculatedMove);
            }
        }
        
        this.gotPossibleMoves = true;
    }

    // Cached heuristic value estimation function
    // TODO: Check whether this function should be part of the board or of an agent, as it is done in the chess and checkers projects
    public int getEstimation() {
        
        if (!this.gotEstimation) {
            this.estimate();
        }
        
        return this.estimation;
    }

    // Heuristic value estimation
    public void estimate() {
        
        this.gotEstimation = true;

        // Easy cases: Already finished games
        if (this.isDecided()) {
            switch (this.getResult()) {
                case 0:
                    this.estimation = SIGNAL_WIN;
                    break;
                case 1:
                    this.estimation = -SIGNAL_WIN;
                    break;
                case 2:
                    this.estimation = 0;
                    break;
                default:
                    System.out.println("===== ERROR: DETECTED END BUT RESULT IS NOT SET ACCORDINGLY =====");
            }
            return;
        }
        
        int res = 0;
        
        this.calculateCompletions();
        
        boolean foundBlocker;
        Field foundDouble = null;
        
        for (int player = 0; player <= 1; player++) {
            for (Field completion : this.completions[player]) {
                for (Field completion2 : this.completions[player]) {
                    if (completion.x == completion2.x && completion.y - completion2.y == -1) {
                        foundBlocker = false;
                        for (Field completionEnemy : this.completions[player^1]) {
                            if (completionEnemy.x == completion.x && completionEnemy.y <= completion.y) {
                                foundBlocker = true;
                                break;
                            }
                        }
                        if (!foundBlocker) {
                            if (foundDouble == null) {
                                foundDouble = completion;
                            } else {
                                int distOld = 0;
                                int distNew = 0;
                                while (foundDouble.y - distOld >= 0 && this.fields[foundDouble.x][foundDouble.y - distOld] < 0) {
                                    distOld++;
                                }
                                while (completion.y - distNew >= 0 && this.fields[completion.x][completion.y - distNew] < 0) {
                                    distNew++;
                                }
                                if (distNew < distOld) {
                                    foundDouble = completion;
                                }
                                else if (distNew == distOld && foundDouble.isCompletion != completion.isCompletion && completion.isCompletion == this.turn) {
                                    foundDouble = completion;
                                }
                            }
                        }
                        break;
                    }
                }
                res += (player == 0 ? WORTH_COMPLETION : -WORTH_COMPLETION);
                if (completion.y % 2 == player) {
                    res += (player == 0 ? COMPLETION_BONUS_ALIGNED : -COMPLETION_BONUS_ALIGNED);
                }
            }
        }
        
        if (foundDouble != null) {
            this.estimation = (foundDouble.isCompletion == 0 ? SIGNAL_SURE : - SIGNAL_SURE);
            return;
        }
        
        int color;
        for (int y = 0; y < this.height; y++) {
            for (int x = 0; x < this.width; x++) {
                color = this.fields[x][y];
                if (color < 0) continue;
                if (color == 0) {
                    res += 3 - Math.abs(x - 3);
                    res += 3 - Math.abs(y - 3);
                    for (int dirX = -1; dirX <= 1; dirX++) {
                        for (int dirY = -1; dirY <= 1; dirY++) {
                            if ((dirX == 0 && dirY == 0) || x + dirX < 0 || x + dirX >= this.width || y + dirY < 0 || y + dirY >= this.height) continue;
                            if (this.fields[x + dirX][y + dirY] == color) {
                                res += 5;
                            }
                            else if (this.fields[x + dirX][y + dirY] < 0) {
                                res += 2;
                            }
                        }
                    }
                }
                else {
                    res -= 3 - Math.abs(x - 3);
                    res -= 3 - Math.abs(y - 3);
                    for (int dirX = -1; dirX <= 1; dirX++) {
                        for (int dirY = -1; dirY <= 1; dirY++) {
                            if ((dirX == 0 && dirY == 0) || x + dirX < 0 || x + dirX >= this.width || y + dirY < 0 || y + dirY >= this.height) continue;
                            if (this.fields[x + dirX][y + dirY] == color) {
                                res -= 5;
                            }
                            else if (this.fields[x + dirX][y + dirY] < 0) {
                                res -= 2;
                            }
                        }
                    }
                }
            }
        }
        
        this.estimation = res;
    }

    // Check which fields can lead to streaks of 4 if filled with a coin of the right color
    // TODO: Re-think what exactly is calculated, maybe write some tests. It seems to work fine and looks sensible, but also a bit convoluted
    public void calculateCompletions() {
        
        this.completions[0].clear();
        this.completions[1].clear();
        
        int dist;
        int foundLength;

        // Go through all fields and use empty fields as the base for calculations
        for (int x = 0; x < this.width; x++) {
            for (int y = 0; y < this.height; y++) {

                // Only consider empty fields
                if (this.fields[x][y] >= 0) continue;
                
                // Horizontal
                // Check for both players if they could reach a streak by filling this field
                for (int player = 0; player <= 1; player++) {
                    foundLength = 0;
                    // Go in both directions for in case this field lies within (not at an end of) a potential streak
                    for (int dirX = -1; dirX <= 1; dirX+= 2) {
                        dist = 0;
                        while (true) {
                            dist++;
                            // Stop at board borders, empty fields and enemy coins
                            if (x + dirX * dist < 0 || x + dirX * dist >= this.width || this.fields[x + dirX * dist][y] != player) break;
                            foundLength++;
                            if (foundLength >= 3) {
                                break;
                            }
                        }
                    }
                    if (foundLength >= 3) {
                        this.completions[player].add(new Field(x, y, player));
                    }
                }
                
                // Vertical
                for (int player = 0; player <= 1; player++) {
                    foundLength = 0;
                    for (int dirY = -1; dirY <= 1; dirY+= 2) {
                        dist = 0;
                        while (true) {
                            dist++;
                            if (y + dirY * dist < 0 || y + dirY * dist >= this.height || this.fields[x][y + dirY * dist] != player) break;
                            foundLength++;
                            if (foundLength >= 3) {
                                break;
                            }
                        }
                    }
                    if (foundLength >= 3) {
                        this.completions[player].add(new Field(x, y, player));
                    }
                }
                
                // Diagonally ascending
                for (int player = 0; player <= 1; player++) {
                    foundLength = 0;
                    for (int dirXY = -1; dirXY <= 1; dirXY+= 2) {
                        dist = 0;
                        while (true) {
                            dist++;
                            if (x + dirXY * dist < 0 || y + dirXY * dist < 0 || x + dirXY * dist >= this.width || y + dirXY * dist >= this.height || this.fields[x + dirXY * dist][y + dirXY * dist] != player) break;
                            foundLength++;
                            if (foundLength >= 3) {
                                break;
                            }
                        }
                    }
                    if (foundLength >= 3) {
                        this.completions[player].add(new Field(x, y, player));
                    }
                }
                
                // Diagonally descending
                for (int player = 0; player <= 1; player++) {
                    foundLength = 0;
                    for (int dirXY = -1; dirXY <= 1; dirXY+= 2) {
                        dist = 0;
                        while (true) {
                            dist++;
                            if (x + dirXY * dist < 0 || y - dirXY * dist < 0 || x + dirXY * dist >= this.width || y - dirXY * dist >= this.height || this.fields[x + dirXY * dist][y - dirXY * dist] != player) break;
                            foundLength++;
                            if (foundLength >= 3) {
                                break;
                            }
                        }
                    }
                    if (foundLength >= 3) {
                        this.completions[player].add(new Field(x, y, player));
                    }
                }
            }
        }
    }

    // Game ending calculation: Check if a streak of 4 exists or the game is tied
    public void checkEnd() {
        
        this.result = -1;
        this.checkedEnd = true;
        
        int foundFour = -1;
        int dist;
        int color;
        
        for (int y = 0; y < this.height; y++) {
            for (int x = 0; x < this.width; x++) {
                color = this.fields[x][y];
                if (color < 0) {
                    continue;
                }
                dist = 0;
                while(true) {
                    dist++;
                    if (x + dist >= this.width || this.fields[x + dist][y] != color) {
                        break;
                    }
                    if (dist >= 3) {
                        foundFour = color;
                        break;
                    }
                }
                if (foundFour >= 0) break;
                dist = 0;
                while(true) {
                    dist++;
                    if (y + dist >= this.height || this.fields[x][y + dist] != color) {
                        break;
                    }
                    if (dist >= 3) {
                        foundFour = color;
                        break;
                    }
                }
                if (foundFour >= 0) break;
                dist = 0;
                while(true) {
                    dist++;
                    if (x + dist >= this.width || y + dist >= this.height || this.fields[x + dist][y + dist] != color) {
                        break;
                    }
                    if (dist >= 3) {
                        foundFour = color;
                        break;
                    }
                }
                if (foundFour >= 0) break;
                dist = 0;
                while(true) {
                    dist++;
                    if (x + dist >= this.width || y - dist < 0 || this.fields[x + dist][y - dist] != color) {
                        break;
                    }
                    if (dist >= 3) {
                        foundFour = color;
                        break;
                    }
                }
                if (foundFour >= 0) break;
            }
            if (foundFour >= 0) break;
        }

        // If streak of 4 was found: According player won
        if (foundFour >= 0) {
            this.result = foundFour;
        }
        // If no streak was found and no moves are possible: Game is tied
        else if (this.getPossibleMoves().isEmpty()) {
            this.result = 2;
        }
        // No win and no tie found => Game is open
    }

    // Cached game ending calculation function
    public int getResult() {
        
        if (!this.checkedEnd) {
            this.checkEnd();
        }
        
        return this.result;
    }
    
    public boolean isDecided() {
                
        return (this.getResult() >= 0);
    }
    
    public int numPieces() {
        
        int res = 0;
        
        for (int y = 0; y < this.height; y++) {
            for (int x = 0; x < this.width; x++) {
                if (this.fields[x][y] != PIECE_NONE) {
                    res ++;
                }
            }
        }
        
        return res;
    }
}
