/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logics;

/**
 *
 * @author Johannes
 */

// Represents a move in the sense of a player putting in a coin
public class Move {
    
    public int column;
    public Board resultingBoard;
    public int estimationLongterm;
    
    public Move(int column) {
        
        this.column = column;
        this.resultingBoard = null;
        this.estimationLongterm = 0;
    }
    
    public int getGain() {
        
        return this.resultingBoard.getEstimation();
    }
}
