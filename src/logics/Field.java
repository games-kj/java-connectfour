/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logics;

/**
 *
 * @author Johannes
 */

// Represents one field of the logic board
public class Field {
    
    public int x;
    public int y;
    public int isCompletion;
    
    public Field(int x, int y, int isCompletion) {
        this.x = x;
        this.y = y;
        this.isCompletion = isCompletion;
    }
    
    public Field(int x, int y) {
        
        this(x, y, -1);
    }
}
